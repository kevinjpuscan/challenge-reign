import React from 'react';
import iconRemove from '../images/trash.svg';
import FormatterTime from '../utils/formatterTime';
import {connect} from 'react-redux';
import * as articleTypes from '../store/types/articleTypes';
import {API_URL} from '../config';

function Article({article,handleRemoveArticle}){
    let removeArticle= async()=>{
        let res=await fetch(`${API_URL}/articles/${article.id}`,{method:'DELETE'});
        if(Number(res.status)===200){
            return true;
        }
        return false;
    }
    let handleClickRemoveArticle=async()=>{
        if(window.confirm('Are you sure delete article?')){
            if(await removeArticle()){
                handleRemoveArticle(article.objectID);
            }
        }
    }
    let handleRedirect=()=>{
        window.location=article.url;
    }
    let createdAt=new FormatterTime(article.createdAt);
    return(<div className="article pointer">
        <div className="article__title" onClick={handleRedirect}>
            <strong>{article.title?article.title:article.storyTitle}</strong>
            <span className="article__author">{article.author}</span>  
        </div>
        <div className="article__time">
            <strong>{createdAt.formater()}</strong>
        </div>
        <div className="article__action" >
            <img className="icon-action" src={iconRemove} alt="remove icon" onClick={handleClickRemoveArticle}/>
        </div>
    </div>);
}

const mapDispatchToFrom=(dispatch)=>{
    return{
        handleRemoveArticle:(objectID)=>dispatch({type:articleTypes.REMOVE_ARTICLE,payload:objectID})
    }
}
export default connect(null, mapDispatchToFrom)(Article);
