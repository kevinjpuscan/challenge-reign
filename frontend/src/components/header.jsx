import React from 'react';
import Wrapper from './wrapper';

export default function Header(){
    let subtitle="We <3 Hacker news!";
    return(
        <header className="header">
            <Wrapper>
                <div className="header__title">
                    <h1>HN Feed</h1>
                </div>
                <div className="header__subtitle">
                    <h3>{subtitle}</h3>
                </div>
            </Wrapper>
        </header>
    );
}
