import React,{useEffect} from 'react';
import Wrapper from './wrapper';
import Article from './article';
import {connect} from 'react-redux';
import * as articleTypes from '../store/types/articleTypes';
import {API_URL} from '../config';

function ArticleList({articles,handleImportArticles}){
    async function getArticles(){
        let response=await fetch(`${API_URL}/articles`);
        let res=await response.json();
        handleImportArticles(res);
    }
    useEffect(()=>getArticles(),[])
    return(
        <div className="article-list">
            <Wrapper>
                {articles.map(article=>(
                    <Article key={article.objectID} article={article}/>
                ))}
            </Wrapper>
        </div>    
        );
}

const mapStateToProps=(state)=>{
    return {
        articles:state.articleReducer.articles
    }
}
const mapDispatchToProps=(dispatch)=>{
    return {
        handleImportArticles:(articles)=>dispatch({type:articleTypes.SET_ARTICLES,payload:articles})
    }
}
export default connect(mapStateToProps,mapDispatchToProps)(ArticleList);