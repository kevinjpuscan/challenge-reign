import './App.css';
import { Provider } from "react-redux";
import reducer from "./store/reducers";
import { createStore } from "redux";
import Header from './components/header';
import ArticleList from './components/articleList';


const store = createStore(reducer);

function App() {
 

  return (
    <Provider store={store}>
    <div className="App">
      <Header />
      <ArticleList />
    </div>
    </Provider>
  );
}

export default App;
