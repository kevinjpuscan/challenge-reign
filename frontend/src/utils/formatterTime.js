const MONTHS=["Jan", "Feb", "Mar", "Apr", "May", "Jun","Jul", "Aug", "Sep", "Oct", "Nov", "Dec"]

export default class FormatterTime{
    constructor(date){
        this.date=new Date(date);
        this.dateNow=new Date();
    }

    formater=()=>{
        if(this.date.getYear()<this.dateNow.getYear()){
            return this.formatBeforeYesterday();
        }
        if(this.date.getMonth()<this.dateNow.getMonth()){
            return this.formatBeforeYesterday();
        }
        if(this.dateNow.getDate()-this.date.getDate()>1){
            return this.formatBeforeYesterday();
        }
        if(this.dateNow.getDate()-this.date.getDate()===1){
            return this.formatYesterday();
        }else{
            return this.formatToday();
        }
    }

    formatBeforeYesterday=()=>{
        return `${MONTHS[this.date.getMonth()]} ${this.date.getDate()}` 
    }

    formatYesterday=()=>{
        return 'Yesterday';
    }

    formatToday=()=>{
        let timeFormatedBase=this.date.toLocaleTimeString('en-US', { timeZoneName: 'short' })
        return this.cleanTime(timeFormatedBase); 
    }

    cleanTime=(timeBase)=>{
        let [time,meridiem]=timeBase.split(' ');
        return `${this.removeMilliSeconds(time)} ${meridiem}`;
    }
    
    removeMilliSeconds=(timeBase)=>{
        let [hours,minutes]=timeBase.split(':');
        return `${hours}:${minutes}`;
    }
}

