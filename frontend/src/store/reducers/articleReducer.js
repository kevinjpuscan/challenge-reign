import * as types from '../types/articleTypes';

const initialState={
    articles:[]
}

export const articleReducer = (state=initialState,action)=>{
switch(action.type){
    case types.SET_ARTICLES:{
        return {...state,articles:action.payload}
    }
    case types.REMOVE_ARTICLE:{
        let newArticles=state.articles.filter(article=>article.objectID!==action.payload);
        return {...state,articles:newArticles};
    }
    default:{
        return state;
    }
}

}