# Challenge REIGN - Articles APP

# Available Scripts

In the project directory, you can run:

#### `docker-compose up`

Open [http://localhost:8080](http://localhost:8080) to view it in the browser.

# Import Article initial
To import articles manually, in the browser visit the path [http://localhost:3000/articles/import/0](http://localhost:3000/articles/import/0)
The final number indicates the page to import, it starts at 0, if you want to import more pages just enter the corresponding number.
