import { Test, TestingModule } from '@nestjs/testing';
import { INestApplication } from '@nestjs/common';
import * as request from 'supertest';
import { AppModule } from '../src/app.module';
import { ArticleService } from 'src/articles/article.services';

describe('Articles Importer (e2e)', () => {
  let app: INestApplication;

  beforeEach(async () => {
    const moduleFixture: TestingModule = await Test.createTestingModule({
      imports: [AppModule, ArticleService],
    }).compile();

    app = moduleFixture.createNestApplication();
    await app.init();
  });

  it('Import articles (GET)', () => {
    return request(app.getHttpServer())
      .get('/articles/import/0')
      .expect(200)
      .expect({ message: 'Articles imported correctly' });
  });

  it('Delete Article without id (POST)', () => {
    return request(app.getHttpServer()).delete('/articles').expect(404);
  });
});
