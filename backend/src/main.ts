import { NestFactory } from '@nestjs/core';
import { AppModule } from './app.module';
import { MongoDBConnection } from './database/mongodb.connection';
import { Config } from './utils/config';

async function bootstrap() {
  new MongoDBConnection();
  const app = await NestFactory.create(AppModule);
  app.enableCors();
  await app.listen(Config.PORT);
}
bootstrap();
