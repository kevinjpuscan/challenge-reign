import { Connection } from 'mongoose';
import { MongoDBConnectionFactory } from './mongodb.connection.factory';
import { Logger } from '@nestjs/common';

export class MongoDBConnection {
  private _connection?: Connection;
  private _connectionFactory: MongoDBConnectionFactory;
  private readonly logger = new Logger(MongoDBConnection.name);

  constructor() {
    this._connectionFactory = new MongoDBConnectionFactory();
    this.tryConnect();
  }

  async tryConnect(): Promise<void> {
    const _this = this;
    await this._connectionFactory
      .createConnection()
      .then((connection: Connection) => {
        this._connection = connection;
        this.connectionStatusListener(this._connection);
      })
      .catch((err) => {
        this.logger.error(err);
        setTimeout(() => {
          _this.tryConnect().then();
        }, this._connectionFactory.getRetryInterval());
      });
  }
  connectionStatusListener(connection: Connection | undefined) {
    if (!connection) return;
    connection.on('conected', () => {
      this.logger.log('MongoDB connection open...');
    });
    connection.on('disconnected', () => {
      this.logger.log('MongoDB connection disconnected...');
    });
  }

  get conn(): Connection | undefined {
    return this._connection;
  }
}
