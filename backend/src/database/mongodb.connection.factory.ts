import { Connection, Mongoose, connect } from 'mongoose';
import { Config } from '../utils/config';

export class MongoDBConnectionFactory {
  private readonly options: object = {
    useNewUrlParser: true,
    useCreateIndex: true,
    useFindAndModify: true,
    bufferMaxEntries: 0,
    reconnectTries: this.getRetryCount(),
    reconnectInterval: this.getRetryInterval(),
  };

  public createConnection(): Promise<Connection> {
    return new Promise<Connection>((resolve, reject) => {
      connect(this.getDBUri(), this.options)
        .then((result: Mongoose) => resolve(result.connection))
        .catch((error) => reject(error));
    });
  }
  getDBUri(): string {
    if (process.env.NODE_ENV && process.env.NODE_ENV === 'dev') {
      return Config.MONGODB_URI_TEST;
    }
    return Config.MONGODB_URI;
  }

  getRetryCount(): number {
    const retryCount = Config.MONGODB_COUNT_RETRY_COUNT;
    return retryCount === 0 ? Number.MAX_SAFE_INTEGER : retryCount;
  }

  getRetryInterval() {
    return Config.MONGODB_COUNT_RETRY_INTERVAL;
  }
}
