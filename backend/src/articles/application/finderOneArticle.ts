import { Query } from '../../shared/query.interface';
import Article from '../domain/Article';
import ArticleRepository from '../domain/ArticleRepository.interface';

export class FinderOneArticle {
  constructor(private repository: ArticleRepository) {}

  exec = async (query: Query): Promise<Article> | undefined => {
    query.pagination.limit = 1;
    return await this.repository.findOne(query);
  };
}
