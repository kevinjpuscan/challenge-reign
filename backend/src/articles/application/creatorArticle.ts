import { BaseQuery } from '../../shared/baseQuery';
import Article from '../domain/Article';
import ArticleRepository from '../domain/ArticleRepository.interface';
import { ApplicationException } from '../exceptions/application.exceptions';
import { FinderOneArticle } from './finderOneArticle';

export class CreatorArticle {
  finderOneArticle: FinderOneArticle;
  constructor(private repository: ArticleRepository) {
    this.finderOneArticle = new FinderOneArticle(this.repository);
  }

  exec = async (article: Article) => {
    let query = new BaseQuery();
    query.filters = { objectID: article.objectID };
    const exist = await this.finderOneArticle.exec(query);
    if (exist.id) {
      throw new ApplicationException('the article already exists');
    }
    this.repository.create(article);
  };
}
