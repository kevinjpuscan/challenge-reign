import ArticleRepository from '../domain/ArticleRepository.interface';
import { ApplicationException } from '../exceptions/application.exceptions';
import { FinderOneArticle } from './finderOneArticle';

export class EliminatorArticle {
  finderOneArticle: FinderOneArticle;
  constructor(private repository: ArticleRepository) {}

  exec = async (id?: string) => {
    if (!id) {
      throw new ApplicationException('param id of article is undefined');
    }

    const article = await this.repository.findById(id);

    if (!article.id) {
      throw new ApplicationException('this article not exists');
    }
    article.deletedAt = new Date();

    this.repository.update(article);
  };
}
