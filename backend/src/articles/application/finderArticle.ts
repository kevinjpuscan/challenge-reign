import { Query } from '../../shared/query.interface';
import Article from '../domain/Article';
import ArticleRepository from '../domain/ArticleRepository.interface';

export class FinderArticle {
  constructor(private repository: ArticleRepository) {}

  exec = async (query: Query): Promise<Array<Article>> | undefined => {
    return await this.repository.find(query);
  };
}
