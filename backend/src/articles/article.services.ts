import { Injectable } from '@nestjs/common';
import axios from 'axios';
import { BaseQuery } from '../shared/baseQuery';
import { CreatorArticle } from './application/creatorArticle';
import { EliminatorArticle } from './application/eliminatorArticle';
import { FinderArticle } from './application/finderArticle';
import ArticleDTO from './database/articleDTO';
import Article from './domain/Article';
import { ArticleMapper } from './infrastructure/ArticleMapper';
import { ArticleMongoRepository } from './infrastructure/ArticleMongoRepository';

@Injectable()
export class ArticleService {
  constructor(
    private repository: ArticleMongoRepository,
    private articleMapper: ArticleMapper,
  ) {}

  async getArticles(): Promise<Array<any>> {
    let query = new BaseQuery();
    query.filters = { deletedAt: null };
    let finderArticle = new FinderArticle(this.repository);
    let articles = await finderArticle.exec(query);
    let articlesJSON = articles.map((article: Article) => {
      return this.articleMapper.modelToJson(article);
    });
    return articlesJSON;
  }

  async importArticles(url: string): Promise<any> {
    console.log(url);
    let creator = new CreatorArticle(this.repository);
    let response = await axios.get(url);
    if (!response) {
      return false;
    }
    let articlesDTO = response.data.hits.map(
      (item) =>
        new ArticleDTO(
          null,
          item.title,
          item.story_title,
          item.author,
          item.story_url,
          Number(item.objectID),
          item.created_at,
        ),
    );

    await articlesDTO.forEach(async (article) => {
      await creator.exec(article);
    });
    return true;
  }
  async save(articleDTO: ArticleDTO) {
    let article = this.articleMapper.DTOtoModel(articleDTO);
    let creator = new CreatorArticle(this.repository);
    await creator.exec(article);
  }

  async delete(id: string) {
    let eliminator = new EliminatorArticle(this.repository);
    await eliminator.exec(id);
  }
}
