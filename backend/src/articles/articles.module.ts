import { Module } from '@nestjs/common';
import { ArticleController } from './article.controller';
import { ArticleService } from './article.services';
import { ArticleTaskService } from './articleTask.services';
import { ArticleMapper } from './infrastructure/ArticleMapper';
import { ArticleMongoRepository } from './infrastructure/ArticleMongoRepository';

@Module({
  controllers: [ArticleController],
  providers: [
    ArticleService,
    ArticleMongoRepository,
    ArticleMapper,
    ArticleTaskService,
  ],
})
export class ArticlesModule {}
