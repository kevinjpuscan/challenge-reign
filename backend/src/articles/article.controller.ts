import {
  Controller,
  Get,
  Post,
  Req,
  Res,
  Body,
  Delete,
  Param,
} from '@nestjs/common';
import { ArticleService } from './article.services';
import { Response, Request } from 'express';
import ArticleDTO from './database/articleDTO';
import { Config } from '../utils/config';

@Controller('articles')
export class ArticleController {
  constructor(private readonly articleService: ArticleService) {}

  @Get()
  async getArticles(@Res() res: Response): Promise<any> {
    try {
      let articles = await this.articleService.getArticles();
      res.json(articles);
    } catch (error) {
      res.status(501).json({ error: error.message });
    }
  }

  @Get('/import/:page')
  async importArticles(@Param() params, @Res() res: Response): Promise<any> {
    try {
      let url = Config.ARTICLE_API_URI;
      if (params.page) {
        url += `&page=${params.page}`;
      }
      let imported = await this.articleService.importArticles(url);
      if (imported) {
        res.json({ message: 'Articles imported correctly' });
      } else {
        res.json({ error: 'Error in import Articles' });
      }
    } catch (error) {
      res.json({ error: 'Error in import Articles' });
    }
  }

  @Post()
  async saveArticle(@Body() articleDTO: ArticleDTO, @Res() res: Response) {
    try {
      await this.articleService.save(articleDTO);
      res.json({ message: 'Article is created' });
    } catch (error) {
      res.status(501).json({ error: error.message });
    }
  }

  @Delete(':id')
  async deleteArticle(@Param() params, @Res() res: Response) {
    if (!params.id) {
      res.status(400).json({ error: 'Param id is required' });
    }
    try {
      await this.articleService.delete(params.id);
      res.json({ message: 'Article is deleted' });
    } catch (error) {
      res.status(501).json({ error: error.message });
    }
  }
}
