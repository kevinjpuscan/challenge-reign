import { Exception } from '../../shared/exception';

export class ApplicationException extends Exception {
  constructor(message: string, description?: string) {
    super(message, description);
  }
}
