import { Schema, Document, model } from 'mongoose';

interface ArticleModel extends Document {}

const ArticleSchema = new Schema(
  {
    title: {
      type: String,
    },
    storyTitle: {
      type: String,
    },
    author: {
      type: String,
    },
    url: {
      type: String,
    },
    objectID: {
      type: Number,
      index: { unique: true },
    },
    createdAt: {
      type: Date,
    },
    deletedAt: {
      type: Date,
    },
  },
  {
    timestamps: {
      createdAt: 'importedAt',
      updatedAt: false,
    },
    toJSON: {
      transform: (doc, ret) => {
        ret.id = ret._id;
        delete ret._id;
        delete ret.__v;
        delete ret.updatedAt;
        return ret;
      },
    },
  },
);

export const ArticleRepoModel = model<ArticleModel>('Article', ArticleSchema);
