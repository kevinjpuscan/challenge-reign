import { Repository } from '../../shared/repository.interface';
import Article from './Article';

export default interface ArticleRepository extends Repository<Article> {}
