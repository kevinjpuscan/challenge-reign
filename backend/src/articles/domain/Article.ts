import { Entity } from '../../shared/entity';

export default class Article extends Entity {
  constructor(
    _id?: string,
    private _title?: string,
    private _storyTitle?: string,
    private _author?: string,
    private _url?: string,
    private _objectID?: number,
    private _createdAt?: Date,
    private _deletedAt?: Date,
    private _importedAt?: Date,
  ) {
    super(_id);
  }

  get title(): string | undefined {
    return this._title;
  }

  set title(title: string | undefined) {
    this._title = title;
  }

  get storyTitle(): string | undefined {
    return this._storyTitle;
  }

  set storyTitle(storyTitle: string | undefined) {
    this._storyTitle = storyTitle;
  }

  get author(): string | undefined {
    return this._author;
  }

  set author(author: string | undefined) {
    this._author = author;
  }

  get url(): string | undefined {
    return this._url;
  }

  set url(url: string | undefined) {
    this._url = url;
  }

  get objectID(): number | undefined {
    return this._objectID;
  }

  set objectID(objectID: number | undefined) {
    this._objectID = objectID;
  }

  get createdAt(): Date | undefined {
    return this._createdAt;
  }

  set createdAt(createdAt: Date | undefined) {
    this._createdAt = createdAt;
  }

  get deletedAt(): Date | undefined {
    return this._deletedAt;
  }

  set deletedAt(deletedAt: Date | undefined) {
    this._deletedAt = deletedAt;
  }

  get importedAt(): Date | undefined {
    return this._importedAt;
  }

  set importedAt(importedAt: Date | undefined) {
    this._importedAt = importedAt;
  }
}
