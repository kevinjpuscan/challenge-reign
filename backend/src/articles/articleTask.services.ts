import { Injectable, Logger } from '@nestjs/common';
import { Cron, CronExpression } from '@nestjs/schedule';
import { Config } from '../utils/config';
import { ArticleService } from './article.services';

@Injectable()
export class ArticleTaskService {
  private readonly logger = new Logger(ArticleTaskService.name);
  constructor(private readonly articleService: ArticleService) {}

  @Cron(CronExpression.EVERY_HOUR)
  async handleCron() {
    this.articleService.importArticles(Config.ARTICLE_API_URI);
    this.logger.debug('Called every 1 hour');
  }
}
