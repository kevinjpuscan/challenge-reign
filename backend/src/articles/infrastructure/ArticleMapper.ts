import { Injectable } from '@nestjs/common';
import { Mapper } from '../../shared/mapper.interface';
import ArticleDTO from '../database/articleDTO';
import Article from '../domain/Article';

@Injectable()
export class ArticleMapper implements Mapper<Article, ArticleDTO> {
  modelToDTO(model: Article): ArticleDTO {
    return new ArticleDTO(
      model.id,
      model.title,
      model.storyTitle,
      model.author,
      model.url,
      model.objectID,
      model.createdAt,
      model.deletedAt,
    );
  }
  DTOtoModel(dto: ArticleDTO): Article {
    return new Article(
      dto.id,
      dto.title,
      dto.storyTitle,
      dto.author,
      dto.url,
      dto.objectID,
      dto.createdAt,
      dto.deletedAt,
      dto.importedAt,
    );
  }

  modelToJson(model: Article): any {
    return {
      id: model.id,
      title: model.title,
      storyTitle: model.storyTitle,
      author: model.author,
      url: model.url,
      objectID: model.objectID,
      createdAt: model.createdAt,
      deletedAt: model.deletedAt ? model.deletedAt : null,
      importedAt: model.importedAt,
    };
  }
}
