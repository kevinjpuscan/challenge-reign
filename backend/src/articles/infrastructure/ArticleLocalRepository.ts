import { Injectable } from '@nestjs/common';
import { Query } from '../../shared/query.interface';
import Article from '../domain/Article';
import ArticleRepository from '../domain/ArticleRepository.interface';

const articleInitial = new Article();
articleInitial.title = 'Article initial';
articleInitial.objectID = 123;
articleInitial.createdAt = new Date();
@Injectable()
export class ArticleLocalRepository implements ArticleRepository {
  findById(id: string): Promise<Article> {
    throw new Error('Method not implemented.');
  }
  articles: Array<Article> = [articleInitial];

  create(item: Article): void {
    this.articles.push(item);
  }

  find(query: Query): Promise<Article[]> {
    return new Promise((resolve, reject) => {
      resolve(this.articles);
    });
  }
  findOne(query: Query): Promise<Article> {
    return new Promise((resolve, reject) => {
      let article = this.articles.find(
        (art: Article) => art.objectID === query.filters['objectID'],
      );
      resolve(article);
    });
  }
  update(item: Article): void {
    throw new Error('Method not implemented.');
  }
}
