import { Mapper } from '../../shared/mapper.interface';
import { ArticleRepoModel } from '../database/article.schema';
import ArticleDTO from '../database/articleDTO';
import Article from '../domain/Article';
import ArticleRepository from '../domain/ArticleRepository.interface';
import { ArticleMapper } from './ArticleMapper';
import { Injectable, Logger } from '@nestjs/common';
import { Query } from '../../shared/query.interface';

@Injectable()
export class ArticleMongoRepository implements ArticleRepository {
  private readonly model: any;
  private readonly mapper: Mapper<Article, ArticleDTO>;
  private readonly logger: Logger;
  constructor() {
    this.model = ArticleRepoModel;
    this.mapper = new ArticleMapper();
    this.logger = new Logger(ArticleMongoRepository.name);
  }
  findById(id: string): Promise<Article> {
    return new Promise<Article>((resolve, reject) => {
      this.model
        .findById(id)
        .exec()
        .then((result: ArticleDTO) => {
          resolve(this.mapper.DTOtoModel(result));
        })
        .catch((err) => reject(err));
    });
  }
  create(item: Article): void {
    let newArticle: ArticleDTO = this.mapper.modelToJson(item);
    this.model
      .create(newArticle)
      .then((result) => {
        this.logger.debug('new Article insert', result);
      })
      .catch((err) => {
        this.logger.error('Error insert article', err);
      });
  }
  find(query: Query): Promise<Article[]> {
    return new Promise<Array<Article>>((resolve, reject) => {
      this.model
        .find(query.filters)
        .select(query.fields)
        .sort(query.ordination)
        .skip(
          query.pagination.limit * query.pagination.page -
            query.pagination.limit,
        )
        .limit(query.pagination.limit)
        .exec()
        .then((result: any) => {
          resolve(
            result.map((item: ArticleDTO) => this.mapper.DTOtoModel(item)),
          );
        })
        .catch((err) => reject(err));
    });
  }
  findOne(query: Query): Promise<Article> {
    return new Promise<Article>((resolve, reject) => {
      this.model
        .find(query.filters)
        .select(query.fields)
        .exec()
        .then((result: ArticleDTO) => {
          resolve(this.mapper.DTOtoModel(result));
        })
        .catch((err) => reject(err));
    });
  }

  update(item: Article): void {
    let articleUpdated: ArticleDTO = this.mapper.modelToJson(item);
    console.log(articleUpdated);
    this.model
      .findOneAndUpdate({ _id: articleUpdated.id }, articleUpdated, {
        new: true,
      })
      .then((result) => {
        this.logger.debug('new Article update', result);
      })
      .catch((err) => {
        this.logger.error('Error update article', err);
      });
  }
  delete(id: string): void {
    throw new Error('Method not implemented.');
  }
  count(query: Query): Promise<number> {
    throw new Error('Method not implemented.');
  }
}
