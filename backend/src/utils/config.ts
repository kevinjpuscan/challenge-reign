export abstract class Config {
  public static readonly PORT: number = Number(process.env.PORT) || 3000;

  public static readonly MONGODB_URI: string =
    process.env.MONGODB_URI || 'mongodb://localhost:27017/app';
  public static readonly MONGODB_URI_TEST: string =
    process.env.MONGODB_URI_TEST || 'mongodb://localhost:27017/app-test';
  public static readonly MONGODB_COUNT_RETRY_COUNT: number =
    Number(process.env.MONGODB_COUNT_RETRY_COUNT) || 0; //infinite
  public static readonly MONGODB_COUNT_RETRY_INTERVAL: number =
    Number(process.env.MONGODB_COUNT_RETRY_INTERVAL) || 1000; //1s

  public static readonly ARTICLE_API_URI: string =
    'https://hn.algolia.com/api/v1/search_by_date?query=nodejs';
}
