import { Query } from './query.interface';

export interface Repository<T> {
  create(item: T): void;
  find(query: Query): Promise<Array<T>>;
  findOne(query: Query): Promise<T>;
  update(item: T): void;
  findById(id: string): Promise<T>;
}
