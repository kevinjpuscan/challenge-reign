export interface Mapper<T, T_DTO> {
  modelToDTO(model: T): T_DTO;
  DTOtoModel(dto: T_DTO): T;
  modelToJson(model: T): any;
}
