import { Serializable } from './serializable.interface';

export interface Pagination extends Serializable<Pagination> {
  page: number;
  limit: number;
}
