import { BasePagination } from './basePagination';
import { Pagination } from './pagination.interface';
import { Query } from './query.interface';

export class BaseQuery implements Query {
  fields: string[];
  ordination: Map<string, string>;
  pagination: Pagination;
  filters: string | object;
  constructor(
    fields?: Array<string>,
    ordination?: Map<string, string>,
    pagination?: Pagination,
    filters?: string | object,
  ) {
    this.fields = fields ? fields : [];
    this.ordination = ordination
      ? ordination
      : new Map().set('createdAt', 'desc');
    this.pagination = pagination ? pagination : new BasePagination();
    this.filters = filters ? filters : {};
  }

  serialize() {
    throw new Error('Method not implemented.');
  }
  deserialize(item: any): Query {
    throw new Error('Method not implemented.');
  }
}
