export abstract class Exception extends Error {
  description: string;
  protected constructor(message: string, description?: string) {
    super(message);
    this.description = description;
  }
}
