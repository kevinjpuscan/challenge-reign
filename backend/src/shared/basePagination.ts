import { Pagination } from './pagination.interface';

export class BasePagination implements Pagination {
  page: number;
  limit: number;

  constructor(page?: number, limit?: number) {
    this.page = page ? page : 1;
    this.limit = limit ? limit : 100;
  }
  serialize() {
    throw new Error('Method not implemented.');
  }
  deserialize(item: any): Pagination {
    throw new Error('Method not implemented.');
  }
}
