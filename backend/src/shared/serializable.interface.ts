export interface Serializable<T> {
  serialize(): any;
  deserialize(item: any): T;
}
