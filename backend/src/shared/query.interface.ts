import { Pagination } from './pagination.interface';
import { Serializable } from './serializable.interface';

export interface Query extends Serializable<Query> {
  fields: Array<string>;
  ordination: Map<string, string>;
  pagination: Pagination;
  filters: object | string;
}
